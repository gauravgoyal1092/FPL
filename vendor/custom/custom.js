// EMPTY VARIABLES
var players = {};
var teams = {};
var player_types = {};

// URLS
var fetchPlayersURL = "https://fantasy.premierleague.com/drf/bootstrap-static";




$(document).ready(function(){
	// var players = fetchData(fetchPlayersURL);
	// console.log("Out here");
	// console.log(players);
	fetchPlayers(fetchPlayersURL)
	var sortedPlayers = sortPlayesByScore(players);
	
	_.each(sortedPlayers, function(player){
	 	player['team'] = getPlayerTeam(player.team);
		player['player_type'] = getPlayerType(player.element_type);

		console.log(player);
	 	var body = createPanelBody(player);
	 	$("#players").append(makePanel(player.web_name, body, player.id));
	});
});




function sortPlayesByScore(data){
	return _(data).chain()
		.sortBy(function(player) {
		    return player.event_points;
		}).sortBy(function(player) {
		    return player.total_points;
		}).value().reverse();
}

function fetchPlayers(url){
	$.ajax({
        url: url,
        type: 'GET',
        cache: false,
        timeout: 30000,
        async: false,
        error: function(){
            console.log("error "+ url);
        },
        success: function(data){
        	console.log(data);
        	players = data.elements;
        	teams = data.teams;
        	player_types = data.element_types;
        }
    });
}


function getPlayerPic(pic_id){
	return "https://platform-static-files.s3.amazonaws.com/premierleague/photos/players/110x140/p"+pic_id.toString()+".png"
}

function getPlayerTeam(team_code){
	return _.find(teams, function(team){
			return team.id == team_code;
	});
}

function getPlayerType(type_id){
	return _.find(player_types, function(type){
			return type.id == type_id;
	});
}

function makePanel(title, body, id){
	var panel =  '<div class="col-md-12" style="margin-bottom: 20px;" id="'+id+'">\
				<div class="card">\
  				<div class="card-header">\
				    '+title+'\
				  </div>\
				  <div class="card-body" style="display: inline-flex;">\
				  	'+body+'\
				  </div></div>\
				</div>';

	return panel;
}

function createPanelBody(player){
	let pic = '<img src='+getPlayerPic(player.photo.split(".")[0])+'  alt='+player.web_name+' />';
	let personal_info = '<h4 class="card-title">'+player.first_name+' '+ player.second_name +'</h4>\
    					<h6 class="card-subtitle mb-2 text-muted"> <span class="badge badge-light">'+player.team.name+ '</span> <span class="badge badge-info">'+ player.player_type.singular_name+'</span></h6>';

    let stats_table = createStatsTable(player);

	return '<div class="col-3">\
				'+pic+'	\
			</div>\
			<div class="col-9">\
				'+personal_info+'</br>\
				'+ stats_table +'\
			</div></div>';
}


function createStatsTable(player){
	let table_cells = [
		['Form', player.form],
		['GW2', player.event_points + "pts"],
		['Total', player.total_points + "pts"],
		['TSB', player.selected_by_percent+ "%"],
		['Influence', player.influence],
		['Creativity', player.creativity],
		['Threat', player.threat],
		['ICT index', player.ict_index]
	];

	let ths = [];
	let tds = [];
	_.each(table_cells, function(cell){
		ths.push('<th>'+cell[0]+'</th>');
		tds.push('<td>'+cell[1]+'</td>');
	});

	return '<table class="table table-inverse">\
			<thead>\
			    <tr>\
			    '+ ths.join('') +'\
			    </tr>\
			  </thead>\
			  <tbody>\
			    <tr>\
			      '+ tds.join('') +'\
			    </tr>\
			    </tbody>\
			</table>';
}